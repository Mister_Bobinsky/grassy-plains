using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Chaser : MonoBehaviour
{
    public GameObject player;
    private NavMeshAgent agent;

    // Update is called once per frame
    void Update()
    {
            //transform.LookAt(player.transform);
            //transform.position += transform.forward * moveSpeed * Time.deltaTime;

            player = GameObject.FindGameObjectWithTag("DestPlayer");

            agent = GetComponent<NavMeshAgent>();

            agent.SetDestination(player.transform.position);
    }
}
