using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class SkeletonChaser : MonoBehaviour
{
    public GameObject player;
    public GameObject temp;
    private NavMeshAgent agent;
    private Animator animator;
    
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
            //transform.LookAt(player.transform);
            //transform.position += transform.forward * moveSpeed * Time.deltaTime;

            player = GameObject.FindGameObjectWithTag("DestPlayer");

            agent = GetComponent<NavMeshAgent>();

            agent.SetDestination(player.transform.position);

            //animator.SetTrigger("Walk1");
    }
}
