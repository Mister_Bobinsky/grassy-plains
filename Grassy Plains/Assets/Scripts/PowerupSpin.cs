using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupSpin : MonoBehaviour
{
    public int spinRate;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0f, spinRate * Time.deltaTime, 0f, Space.Self);
    }
}
