using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitFPS : MonoBehaviour
{
    public int frameLimit;

    // Update is called once per frame
    void Update()
    {
        Application.targetFrameRate = frameLimit;
    }
}
